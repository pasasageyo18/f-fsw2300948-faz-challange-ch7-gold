const express = require("express");
const app = express();
const { sequelize } = require("./models/index");
const bodyParser = require("body-parser");
const time = require("./middleware/index");
const passport = require("passport");
const indexRoutes = require("./routes/index");
const trialRoutes = require("./routes/trial");
const authRoutes = require("./routes/auth");
const dashboardRoutes = require("./routes/dashboard");
const userRoutes = require("./routes/users");
const userBioRoutes = require("./routes/userBio");

// CONFIGURATIONS
app.use(bodyParser.urlencoded({ extended: true }));
app.use(express.json());
app.use(time);
app.use(passport.initialize()); //initialize passport for auth strategy
require("./lib/passport");
app.use(express.static("client/public"));
app.set("view engine", "ejs");

// ROUTES
app.use(indexRoutes);
app.use(trialRoutes);
app.use(authRoutes);
app.use(dashboardRoutes);
app.use(userRoutes);
app.use(userBioRoutes);

// SERVER
app.listen({ port: 3000 }, async () => {
  console.log("Server up on http://localhost:3000");
  await sequelize.authenticate();

  console.log("Database Connected!");
});

// ONE TIME USE FOR DROP AND SYNC ALL TABLE AGAIN
// (async () => {
//   try {
//     await sequelize.sync({ force: true });

//     console.log("Database sync successful.");
//   } catch (error) {
//     console.error("Error syncing database:", error);
//   }
// })();
