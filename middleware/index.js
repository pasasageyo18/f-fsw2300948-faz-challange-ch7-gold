const express = require("express");
const router = express.Router();

// SET TIMELOG AS MIDDLEWARE
router.use(function timeLog(req, res, next) {
  console.log("Time: ", Date.now());
  next();
});

module.exports = router;
