const passport = require("passport");
const JwtStrategy = require("passport-jwt").Strategy;
const ExtractJwt = require("passport-jwt").ExtractJwt;
const { User_game } = require("../models");

const jwtOptions = {
  jwtFromRequest: ExtractJwt.fromHeader("authorization"),
  secretOrKey: "this-is-secret",
};

passport.use(
  new JwtStrategy(jwtOptions, (payload, done) => {
    console.log("JWT Payload:", payload);
    User_game.findByPk(payload.id)
      .then((user) => {
        if (user) {
          done(null, user);
        } else {
          console.error("error", err);
          done(null, false);
        }
      })
      .catch((err) => {
        console.error("error", err);
        done(err, false);
      });
  })
);

passport.serializeUser((user, done) => {
  done(null, user.id);
});

passport.deserializeUser((id, done) => {
  User_game.findByPk(id)
    .then((user) => {
      done(null, user);
    })
    .catch((err) => {
      done(err);
    });
});

module.exports = passport;
