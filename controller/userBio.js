const { User_game_biodata } = require("../models");

// GET THE FORM TO UPDATE BIO AND PASS THE EXISTING DATA TO THE FORM VALUE
const getFormUpdateBio = (req, res) => {
  User_game_biodata.findOne({ where: { id: req.params.id } }).then((bio) => {
    res.render("update-bio", {
      id: bio.id,
      name: bio.name,
      email: bio.email,
      gender: bio.gender,
      dateOfBirth: bio.dateOfBirth,
      city: bio.city,
    });
  });
};

// POST THE UPDATED BIO DATA TO DATABASE
const updateBio = (req, res) => {
  const { name, email, gender, dateOfBirth, city } = req.body;
  User_game_biodata.update(
    { name, email, gender, dateOfBirth, city },
    { where: { id: req.params.id } }
  )
    .then((user) => {
      console.log("user successfully updated!");
      res.redirect("/dashboard-user-game-biodata");
    })
    .catch((err) => {
      res.status(500).json("Can't update an user", err);
    });
};

// EXPORT ALL THE FUNCTIONS
exports.getFormUpdateBio = getFormUpdateBio;
exports.updateBio = updateBio;
