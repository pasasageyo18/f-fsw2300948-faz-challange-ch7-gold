const {
  User_game,
  User_game_biodata,
  User_game_history,
  Room,
} = require("../models");

const UserRoom = require("../models").UserRoom;

// GET ALL OF THE USER_GAME AND PASS IT TO THE EJS FILE
const getDashboardUser = (req, res) => {
  User_game.findAll().then((users) => {
    res.render("dashboard-user-game", {
      users,
    });
  });
};

const getUserDetails = async (req, res) => {
  try {
    if (req.user.role !== "superadmin") {
      return res.status(403).json({ message: "Only superadmin can access" });
    }

    const userId = req.params.id;
    const user = await User_game.findOne({
      where: { id: userId },
      include: [
        {
          model: User_game_biodata,
          as: "biodata",
        },
        {
          model: User_game_history,
          as: "history",
        },
      ],
    });
    if (!user) return res.status(404).json({ message: "User not found!" });

    res.status(201).json(user);
  } catch (err) {
    res.status(500).json({ message: err.message });
  }
};

// GET ALL OF THE BIODATA ATTRIBUTES IN THE USER GAME AND PASS IT TO THE EJS FILE
const getDashboardUserBio = async (req, res) => {
  try {
    if (req.user.role !== "superadmin") {
      return res.status(403).json({ message: "Only superadmin can access" });
    }
    const biodata = await User_game.findAll({
      include: [
        {
          model: User_game_biodata,
          as: "biodata",
          attributes: [
            "id",
            "uuid",
            "userGameId",
            "name",
            "email",
            "gender",
            "dateOfBirth",
            "city",
          ],
        },
      ],
    });
    res.render("dashboard-user-game-biodata", { biodata });
  } catch (err) {
    res.status(500).json(err);
  }
};

// GET ALL OF THE HISTORY IN THE USER GAME AND PASS IT TO THE EJS FILE
const getDashboardUserHistory = async (req, res) => {
  try {
    if (req.user.role !== "superadmin") {
      return res.status(403).json({ message: "Only superadmin can access" });
    }
    const history = await User_game_history.findAll({
      include: { model: User_game, as: "historyId", attributes: ["id"] },
    });

    res.render("dashboard-user-game-history", { history });
  } catch (err) {
    console.error(err);
  }
};

// GET THE FORM TO CREATE USER AND BIO DATA
const getCreate = (req, res) => {
  res.render("create");
};

// REQUEST AND POST THE BIO AND USER DATA TO THE DATABASE
const postCreate = async (req, res) => {
  try {
    if (req.user.role !== "superadmin") {
      return res.status(403).json({ message: "Only superadmin can access" });
    }

    const { username, password, role, name, email, gender, dateOfBirth, city } =
      req.body;
    const user = await User_game.create({
      username,
      password,
      role,
    });

    const biodata = await User_game_biodata.create({
      userGameId: user.id,
      name,
      email,
      gender,
      dateOfBirth,
      city,
    });

    const history = await User_game_history.create({
      userGameHistoryId: user.id,
    });
    res.redirect("/dashboard-user-game");
  } catch (err) {
    console.error(err);
  }
};

// GET THE FORM TO UPDATE USER AND PASS THE EXISTING DATA TO THE FORM VALUE
const getFormUpdate = (req, res) => {
  if (req.user.role !== "superadmin") {
    return res.status(403).json({ message: "Only superadmin can access" });
  }
  User_game.findOne({ where: { id: req.params.id } }).then((user) => {
    res.render("update", {
      id: user.id,
      username: user.username,
      password: user.password,
      role: user.role,
    });
  });
};

// POST THE UPDATED USER DATA TO DATABASE
const updateUser = (req, res) => {
  if (req.user.role !== "superadmin") {
    return res.status(403).json({ message: "Only superadmin can access" });
  }

  User_game.update(
    {
      username: req.body.username,
      password: req.body.password,
      role: req.body.role,
    },
    {
      where: { id: req.params.id },
    }
  )
    .then((user) => {
      console.log("user successfully updated!");
      res.redirect("/dashboard-user-game");
    })
    .catch((err) => {
      res.status(500).json("Can't update an user", err);
    });
};

// DELETE THE USER DATA
const deleteUser = (req, res) => {
  User_game.destroy({ where: { id: req.params.id } }).then((user) => {
    res.render("delete");
  });
};

// CREATE ROOM FOR SUPERADMIN ONLY
const createRoom = async (req, res) => {
  try {
    if (req.user.role !== "superadmin") {
      return res
        .status(403)
        .json({ message: "Only superadmin can create rooms" });
    }

    const { roomName, description } = req.body;
    const room = await Room.create({
      roomName,
      description,
      ownerId: req.user.id,
    });
    res.status(201).json({ message: "Room has been created!", room });
  } catch (err) {
    res.status(500).json({ error: err.message });
  }
};

// CHECK ALL ROOMS ON SUPERADMIN DASHBOARD
const getRooms = async (req, res) => {
  try {
    const rooms = await Room.findAll();
    res.status(200).json({ rooms });
  } catch (error) {
    res.status(500).json({ error: error.message });
  }
};

// GET ROOM INFORMATION
const getRoomDetails = async (req, res) => {
  try {
    const room = await Room.findByPk(req.params.roomId);
    const participants = await room.getParticipants();

    res.status(200).json({ participants });
  } catch (err) {
    res.status(500).json({ error: err.message });
  }
};

// DELETE ROOM
const deleteRoom = async (req, res) => {
  try {
    const roomId = req.params.id;
    const room = await Room.findByPk(roomId);

    if (!room) {
      return res.status(404).json({ message: "Room not found" });
    }

    // Delete the room
    await room.destroy();

    res.status(200).json({ message: "Room deleted successfully" });
  } catch (err) {
    res.status(500).json({ error: err.message });
  }
};

// EXPORT ALL THE FUNCTIONS
exports.getDashboardUser = getDashboardUser;
exports.getDashboardUserBio = getDashboardUserBio;
exports.getDashboardUserHistory = getDashboardUserHistory;
exports.getCreate = getCreate;
exports.postCreate = postCreate;
exports.getUserDetails = getUserDetails;
exports.getFormUpdate = getFormUpdate;
exports.updateUser = updateUser;
exports.deleteUser = deleteUser;
exports.createRoom = createRoom;
exports.getRooms = getRooms;
exports.getRoomDetails = getRoomDetails;
exports.deleteRoom = deleteRoom;
