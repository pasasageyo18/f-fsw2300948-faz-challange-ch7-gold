const bcrypt = require("bcrypt");
const jwt = require("jsonwebtoken");
const {
  User_game,
  User_game_biodata,
  User_game_history,
} = require("../models");

// GET THE LOGIN FORM
const getLogin = (req, res) => {
  res.render("login");
};

// POST THE REQUESTED DATA TO BODY AND VERIFY THE USER
const postLogin = async (req, res) => {
  try {
    const { username, password } = req.body;
    const user = await User_game.findOne({ where: { username: username } });
    if (!user) return res.status(401).json({ message: "User not found" });

    const passwordMatch = await bcrypt.compare(password, user.password);
    if (!passwordMatch)
      return res.status(401).json({ message: "Invalid password" });

    const token = generateToken(user); // after done checking, token will be generated

    res.status(201).json({ token: token });
  } catch (err) {
    console.error(err);
    res.status(500).json({ error: err.message });
  }
};

// GET REGISTER PAGE
const getRegister = (req, res) => {
  res.render("register");
};

// POST REGISTER
const postRegister = async (req, res) => {
  try {
    const { username, role, password, name, email, gender, dateOfBirth, city } =
      req.body;

    const user = await User_game.findOne({ where: { username: username } });
    if (user)
      return res.status(401).json({ message: "Username already used!" });

    const passwordHash = await bcrypt.hash(password, 10); // password will be encrypted

    const newUser = await User_game.create({
      username,
      password: passwordHash,
      role,
    });

    const biodata = await User_game_biodata.create({
      userGameId: newUser.id,
      name,
      email,
      gender,
      dateOfBirth,
      city,
    });

    const history = await User_game_history.create({
      userGameHistoryId: newUser.id,
    });

    res.status(201).json({ message: "User successfully created!", newUser });
  } catch (err) {
    res.status(500).json({ error: err.message });
  }
};

// FUNCTION TO GENERATE TOKEN
const generateToken = (user) => {
  const payload = {
    id: user.id,
    username: user.username,
  };
  return jwt.sign(payload, "this-is-secret", { expiresIn: "1h" });
};

// EXPORT ALL THE FUNCTIONS
exports.getLogin = getLogin;
exports.postLogin = postLogin;
exports.getRegister = getRegister;
exports.postRegister = postRegister;
