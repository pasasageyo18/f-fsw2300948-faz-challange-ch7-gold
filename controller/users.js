const {
  User_game,
  User_game_biodata,
  User_game_history,
  Room,
} = require("../models");

// GET USER PAGE
const getUserPage = async (req, res) => {
  try {
    const token = req.query.token;
    const user = req.user;
    res.json({ user, token });
  } catch (err) {
    res.status(500).json({ error: err.message });
    console.error("error", err);
  }
};

// GET ALL ROOMS
const getRooms = async (req, res) => {
  try {
    const rooms = await Room.findAll();
    res.status(200).json(rooms);
  } catch (error) {
    res.status(500).json({ error: error.message });
  }
};

// JOIN ROOM
const joinRoom = async (req, res) => {
  try {
    const user = req.user; //take the payload from token
    const roomId = req.params.roomId;
    const room = await Room.findByPk(roomId);
    if (!room) {
      return res.status(404).json({ message: "Room not found" });
    }

    if (room.currentPlayers >= room.capacity) {
      return res.status(400).json({ message: "Room is full" });
    }

    const alreadyJoined = await user.hasJoinedRoom(room);
    if (alreadyJoined) {
      return res.status(400).json({ message: "User is already in the room" });
    }

    await user.addJoinedRoom(room); // Allow the user to join
    room.increment("currentPlayers"); // Increment currentPlayers count
    return res.status(200).json({ message: "User joined the room", room });
  } catch (err) {
    res.status(500).json({ error: err.message });
    console.error("error", err);
  }
};

exports.getUserPage = getUserPage;
exports.getRooms = getRooms;
exports.joinRoom = joinRoom;
