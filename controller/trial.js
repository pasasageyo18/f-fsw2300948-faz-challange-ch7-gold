const { User_game, Room, User_game_history } = require("../models");

// GET THE TRIAL PAGE
const getTrial = (req, res) => {
  res.render("trial");
};

// GAME FUNCTION
const playGame = async (req, res) => {
  try {
    const roomId = req.params.roomId;
    const room = await Room.findByPk(roomId, {
      include: [{ model: User_game, as: "participants" }],
    });

    if (!room) {
      return res.status(404).json({ message: "Room not found" });
    }

    const userId = req.user.id;
    const choice = req.body.choice;

    // Check if the user is a participant in the room
    const isParticipant = room.participants.some(
      (participant) => participant.id === userId
    );
    if (!isParticipant) {
      return res
        .status(403)
        .json({ message: "You are not a player in this room" });
    }

    // To prevent players from make the choice again
    if (userId === room.participants[0].id && room.player1Choice) {
      return res
        .status(400)
        .json({ message: "You have already made a choice" });
    }

    if (userId === room.participants[1].id && room.player2Choice) {
      return res
        .status(400)
        .json({ message: "You have already made a choice" });
    }

    if (userId === room.participants[0].id) {
      // User is player 1
      room.player1Choice = choice;
    } else if (userId === room.participants[1].id) {
      // User is player 2
      room.player2Choice = choice;
    } else {
      return res.status(400).json({ message: "Invalid action" });
    }

    await room.save();

    if (room.player1Choice && room.player2Choice) {
      // Determine the winner and update the room accordingly
      const winner = gameLogic(room.player1Choice, room.player2Choice);
      room.winner = winner;

      // Update user history based on game result
      const player1Id = room.participants[0].id;
      const player2Id = room.participants[1].id;

      if (winner === "player1") {
        await updateUserHistory(player1Id, true);
        await updateUserHistory(player2Id, false);
      } else if (winner === "player2") {
        await updateUserHistory(player1Id, false);
        await updateUserHistory(player2Id, true);
      } else {
        // It's a draw, no need to update history
      }

      console.log(room.winner, "wins!");
      // reset the room
      room.player1Choice = null;
      room.player2Choice = null;
      room.winner = null;
      await room.save();
    }

    res.status(201).json(room);
  } catch (err) {
    res.status(500).json({ error: err.message });
  }
};

const gameLogic = (player1Choice, player2Choice) => {
  if (player1Choice === player2Choice) {
    return "draw";
  } else if (
    (player1Choice === "rock" && player2Choice === "scissor") ||
    (player1Choice === "paper" && player2Choice === "rock") ||
    (player1Choice === "scissor" && player2Choice === "paper")
  ) {
    return "player1"; // Player 1 wins
  } else {
    return "player2"; // Player 2 wins
  }
};

const updateUserHistory = async (userId, isWinner) => {
  try {
    const userHistory = await User_game_history.findOne({
      where: { userGameHistoryId: userId },
    });

    if (!userHistory) {
      return; // User history not found
    }

    if (isWinner) {
      userHistory.win += 1;
      userHistory.timePlay = new Date();
    } else {
      userHistory.lose += 1;
      userHistory.timePlay = new Date();
    }

    userHistory.totalMatch += 1;
    await userHistory.save();
  } catch (err) {
    console.error("Error updating user history:", err);
  }
};

// EXPORT ALL THE FUNCTIONS
exports.getTrial = getTrial;
exports.playGame = playGame;
