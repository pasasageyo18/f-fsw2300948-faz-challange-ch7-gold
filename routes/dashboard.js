const express = require("express");
const router = express.Router();
const dashboard = require("../controller/dashboard");
const passport = require("passport");

// CREATE
router.post(
  "/dashboard-user-game/create-new",
  passport.authenticate("jwt", { session: false }),
  dashboard.postCreate
);
router.post(
  "/dashboard-rooms/create-new",
  passport.authenticate("jwt", { session: false }),
  dashboard.createRoom
);

// READ
router.get(
  "/dashboard-user-game",
  passport.authenticate("jwt", { session: false }),
  dashboard.getDashboardUser
);
router.get(
  "/dashboard-user-game-biodata",
  passport.authenticate("jwt", { session: false }),
  dashboard.getDashboardUserBio
);
router.get(
  "/dashboard-user-game-history",
  passport.authenticate("jwt", { session: false }),
  dashboard.getDashboardUserHistory
);
router.get(
  "/dashboard-user-game/:id",
  passport.authenticate("jwt", { session: false }),
  dashboard.getUserDetails
);
router.get(
  "/dashboard-user-game/create-new-user",
  passport.authenticate("jwt", { session: false }),
  dashboard.getCreate
);
router.get(
  "/dashboard-user-game/update/(:id)",
  passport.authenticate("jwt", { session: false }),
  dashboard.getFormUpdate
);
router.get(
  "/dashboard-rooms",
  passport.authenticate("jwt", { session: false }),
  dashboard.getRooms
);
router.get(
  "/dashboard-rooms/:roomId",
  passport.authenticate("jwt", { session: false }),
  dashboard.getRoomDetails
);

// UPDATE
router.post(
  "/dashboard-user-game/update/(:id)",
  passport.authenticate("jwt", { session: false }),
  dashboard.updateUser
);

// DELETE
router.get(
  "/dashboard-user-game/delete/(:id)",
  passport.authenticate("jwt", { session: false }),
  dashboard.deleteUser
);
router.delete(
  "/dashboard-rooms/delete/:id",
  passport.authenticate("jwt", { session: false }),
  dashboard.deleteRoom
);

module.exports = router;
