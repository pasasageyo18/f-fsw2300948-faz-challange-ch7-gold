const express = require("express");
const router = express.Router();
const trial = require("../controller/trial");
const passport = require("passport");

// READ
// router.get("/trial", trial.getTrial);

// UPDATE
router.post(
  "/user-page/room/:roomId/play-game",
  passport.authenticate("jwt", { session: false }),
  trial.playGame
);

module.exports = router;
