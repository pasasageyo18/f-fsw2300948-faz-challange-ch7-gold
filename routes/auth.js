const express = require("express");
const router = express.Router();
const auth = require("../controller/auth");

// READ
router.get("/register", auth.getRegister);
router.get("/login", auth.getLogin);

// CREATE
router.post("/register", auth.postRegister);
router.post("/login", auth.postLogin);

module.exports = router;
