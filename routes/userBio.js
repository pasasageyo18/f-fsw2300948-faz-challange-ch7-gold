const express = require("express");
const router = express.Router();
const userBio = require("../controller/userBio");
const passport = require("passport");

// READ
router.get(
  "/dashboard-user-game-biodata/create-new-user-bio",
  passport.authenticate("jwt", { session: false }),
  userBio.getFormUpdateBio
);
router.get(
  "/dashboard-user-game-biodata/update/(:id)",
  passport.authenticate("jwt", { session: false }),
  userBio.getFormUpdateBio
);

//UPDATE
router.post(
  "/dashboard-user-game-biodata/update/(:id)",
  passport.authenticate("jwt", { session: false }),
  userBio.updateBio
);

module.exports = router;
