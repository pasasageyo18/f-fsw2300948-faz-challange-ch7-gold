const express = require("express");
const router = express.Router();
const user = require("../controller/users");
const passport = require("passport");

router.get(
  "/user-page",
  passport.authenticate("jwt", { session: false }),
  user.getUserPage
);

router.get(
  "/user-page/rooms",
  passport.authenticate("jwt", { session: false }),
  user.getRooms
);

router.post(
  "/join-rooms/:roomId",
  passport.authenticate("jwt", { session: false }),
  user.joinRoom
);

module.exports = router;
