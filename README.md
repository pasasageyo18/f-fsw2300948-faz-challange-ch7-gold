# Step by step how to use the project

# Attention

Before following the steps, make sure to check [my postman collection](https://www.postman.com/pasasa18/workspace/rps-game-project) for all the endpoints in the project.

## 1. Create the database and install all the packages

You must create the database and install all the packages using this command:

```bash
sequelize db:create
npm install
```

## 2. Migrate all the models

Migrating all the models into the database

```bash
sequelize db:migrate
```

## 3. Uncomment the sync function in index.js and start the project

```javascript
(async () => {
  try {
    await sequelize.sync({ force: true });

    console.log("Database sync successful.");
  } catch (error) {
    console.error("Error syncing database:", error);
  }
})();
```

This function will drop all the tables and make a new one but with the association. It's in the index.js module.

## 4. Comment again the sync function and restart your project

Comment the sync function again so the table will not be dropped again every time we start the project. Next, restart your project

## 5. Create 3 dummy account in register endpoint

After restarting the project, go to register endpoint and make three dummy account to try the project. One with the "superadmin" role, two with the "user" role in postman.
This is the json looks like for the request body:

```json
{
  "username": "your username",
  "role": "your role",
  "password": "your password",
  "name": "your name",
  "email": "your email",
  "gender": "your gender",
  "dateOfBirth": "the date when you where born",
  "city": "your city"
}
```

If it's not worked, try using browser instead because I have prepared the EJS for register and login.
If it's worked, the data will be registered to "user_games", "user_game_biodata", and "user_game_history" table.

If you already created all of the accounts and want to check any specific user detail, you can go to the /dashboard-user-game/:id endpoint. The ":id" must be replace with the user id. In order to do that, you must login first using the "superadmin" role account.

## 6. Login with superadmin account

After registered all the accounts, you can try to login with the "superadmin" role in the login endpoint.
This is the json looks like for the request body:

```json
{
  "username": "your superadmin account username",
  "password": "your superadmin account password"
}
```

If it's not worked or there's an error like incorrect username or password, it will return a json with the error message.
If it's worked, it will generate a token and return it into a json.

## 7. Create room using superadmin account

After login with the superadmin account, access the create room endpoint /dashboard-rooms/create-new to create a room.
This is the json looks like for the request body:

```json
{
  "roomName": "room name",
  "description": "the description"
}
```

Don't forget to copy and paste the token into request header, write the Authorization and put the token in that value.

If it's worked, it will return the created room json.
If it's not, it will return the error message json.

After that, you can check all rooms that you've been created in dashboard-rooms endpoint.

## 8. Login with two "user" role account

After creating room, you can login again with two user account. It will work the same as superadmin. But, the user cannot access the dashboard endpoint.

## 9. Join a room using the user account

Join a room by using the join room endpoint. You can go to "/join-rooms/:roomId", the ":roomId" must be replace with the room id.
The method will me POST and don't forget to put the token in the request header.

If it's worked, it will return the a success message with the room that have been joined into a json.
If it's not, it will return the error message json.

## 10. Make the choice for RPS on every players

After two players joined the room, they can play the RPS game. Go to "/user-page/room/:roomId/play-game" endpoint and make sure to put the token. POST the json to request body.

This is the json looks like for the request body:

```json
{
  "choice": "rock/paper/scissor"
}
```

After player one POST the choice, it will return the room with the updated data on the playerOneChoice. It will fill with the choice. After player two POST the choice, the room data will be updated with playerOneChoice and playerTwoChoice value is null, because the value is going to reset after the game got the winner.

## 11. Check the user_game_history table

After the game get who win and lose, the result will be stored and updating the players history. Next, you can check the user_game_history table for the updated history on both players.

# That's all!

If anything goes wrong, you can contact me. Thank you!
