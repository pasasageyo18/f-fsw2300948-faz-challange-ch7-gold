"use strict";
const { Model } = require("sequelize");
module.exports = (sequelize, DataTypes) => {
  class Room extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
      Room.belongsTo(models.User_game, { as: "owner", foreignKey: "ownerId" });
      Room.belongsToMany(models.User_game, {
        through: "UserRoom",
        as: "participants",
        foreignKey: "roomId",
      });
    }
  }
  Room.init(
    {
      roomName: {
        type: DataTypes.STRING,
        allowNull: false,
      },
      ownerId: {
        type: DataTypes.INTEGER,
        allowNull: false,
        onDelete: "CASCADE",
        references: {
          model: "user_games",
          key: "id",
        },
      },
      description: {
        type: DataTypes.STRING,
        allowNull: false,
      },
      capacity: {
        type: DataTypes.INTEGER,
        allowNull: false,
        defaultValue: 2,
      },
      currentPlayers: {
        type: DataTypes.INTEGER,
        allowNull: false,
        defaultValue: 0,
      },
      player1Choice: {
        type: DataTypes.STRING,
        allowNull: true,
      },
      player2Choice: {
        type: DataTypes.STRING,
        allowNull: true,
      },
      winner: {
        type: DataTypes.STRING,
        allowNull: true,
      },
    },
    {
      sequelize,
      modelName: "Room",
      tableName: "rooms",
    }
  );
  return Room;
};
